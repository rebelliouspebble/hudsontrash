﻿namespace HudsonTrash
{
    partial class Landing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnScores = new System.Windows.Forms.Button();
            this.BtnPlay = new System.Windows.Forms.Button();
            this.BtnLogout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            this.btnScores.Location = new System.Drawing.Point(14, 57);
            this.btnScores.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnScores.Name = "btnScores";
            this.btnScores.Size = new System.Drawing.Size(247, 338);
            this.btnScores.TabIndex = 0;
            this.btnScores.Text = "View Scores";
            this.btnScores.UseVisualStyleBackColor = true;
            this.btnScores.Click += new System.EventHandler(this.btnScores_Click);
            this.BtnPlay.Location = new System.Drawing.Point(271, 59);
            this.BtnPlay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BtnPlay.Name = "BtnPlay";
            this.BtnPlay.Size = new System.Drawing.Size(282, 333);
            this.BtnPlay.TabIndex = 1;
            this.BtnPlay.Text = "Play";
            this.BtnPlay.UseVisualStyleBackColor = true;
            this.BtnPlay.Click += new System.EventHandler(this.BtnPlay_Click);
            this.BtnLogout.Location = new System.Drawing.Point(467, 13);
            this.BtnLogout.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BtnLogout.Name = "BtnLogout";
            this.BtnLogout.Size = new System.Drawing.Size(85, 24);
            this.BtnLogout.TabIndex = 2;
            this.BtnLogout.Text = "Logout";
            this.BtnLogout.UseVisualStyleBackColor = true;
            this.BtnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 459);
            this.Controls.Add(this.BtnLogout);
            this.Controls.Add(this.BtnPlay);
            this.Controls.Add(this.btnScores);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Landing";
            this.Text = "Landing";
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btnScores;
        private System.Windows.Forms.Button BtnPlay;
        private System.Windows.Forms.Button BtnLogout;
    }
}