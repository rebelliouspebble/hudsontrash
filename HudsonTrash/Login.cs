﻿using System;
using System.Data.SQLite;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace HudsonTrash
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        public string GenerateHash(string username, string password)
        {
            // Generates a password hash using both the username and password
            // Password hash is salted with hash of username, and the combination is hashed together

            var sha256 = SHA256.Create();

            var userhash = sha256.ComputeHash(Encoding.ASCII.GetBytes(username));
            var passhash = sha256.ComputeHash(Encoding.ASCII.GetBytes(password));

            var hashcombo = new byte[65];
            var hasharray = hashcombo.ToList();

            for (var i = 0; i <= userhash.Length - 1; i++)
            {
                hasharray.Add(userhash[i]);
                hasharray.Add(passhash[i]);
            }

            hashcombo = hasharray.ToArray();

            var finalhash = sha256.ComputeHash(hashcombo);

            return Encoding.ASCII.GetString(finalhash);
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
            var db = new Database(dbConnection);

            var userExists = false;
            try
            {
                foreach (var user in db.Users)
                    if (user.username == TxtUsername.Text)
                        userExists = true;
            }
            catch (SQLiteException exception)
            {
                Console.WriteLine(exception.Data.ToString());
                Console.WriteLine(exception.ErrorCode);
                Console.WriteLine(exception.HelpLink);
                Console.WriteLine(exception.InnerException);
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.StackTrace);
                Console.WriteLine(exception.TargetSite);
                Console.WriteLine(exception.ToString());

                db.ExecuteCommand(@"create table Users
(
    id       integer not null
        primary key,
    username text    not null,
    password text    not null
);

create table Scores
(
    id     integer
        constraint Scores_pk
            primary key,
    score  integer,
    date   text,
    userID integer
        references Users
);
                ");
            }

            switch (userExists)
            {
                case true:
                {
                    var username = TxtUsername.Text;

                    var passwordhash = GenerateHash(TxtUsername.Text, TxtPassword.Text);

                    var userQuery =
                        from user in db.Users
                        where user.username == username && user.password == passwordhash
                        select user;


                    if (userQuery.Any())
                    {
                        Console.WriteLine("Login Successful for " + TxtUsername.Text);
                        Hide();
                        var Landing = new Landing(userQuery.ToList().First().id.Value);
                        Landing.Closed += (s, args) => Close();
                        Landing.Show();
                    }
                    else
                    {
                        MessageBox.Show("Incorrect Password");
                    }

                    break;
                }


                case false:
                {
                    var passwordhash = GenerateHash(TxtUsername.Text, TxtPassword.Text);

                    var newuser = new User
                    {
                        password = passwordhash,
                        username = TxtUsername.Text
                    };
                    db.Users.InsertOnSubmit(newuser);

                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (SQLiteException exception)
                    {
                        Console.WriteLine(exception.Data.ToString());
                        Console.WriteLine(exception.ErrorCode);
                        Console.WriteLine(exception.HelpLink);
                        Console.WriteLine(exception.InnerException);
                        Console.WriteLine(exception.Message);
                        Console.WriteLine(exception.StackTrace);
                        Console.WriteLine(exception.TargetSite);
                        Console.WriteLine(exception.ToString());

                        db.SubmitChanges();
                    }

                    var username = TxtUsername.Text;

                    var userQuery =
                        from user in db.Users
                        where user.username == username && user.password == passwordhash
                        select user;


                    if (userQuery.Any())
                    {
                        Console.WriteLine("Login Successful for " + TxtUsername.Text);
                        Hide();
                        var Landing = new Landing(userQuery.ToList().First().id.Value);
                        Landing.Closed += (s, args) => Show();
                        Landing.Show();
                    }


                    break;
                }
            }
        }
    }
}