﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace HudsonTrash
{
    public partial class Quiz : Form
    {
        public enum Topic
        {
            Geography,
            Maths,
            CS
        }

        private readonly int id;

        private readonly XmlSerializer ser = new XmlSerializer(typeof(Topics));

        private readonly Stats stats;
        private readonly Topic topic;

        private List<Button> answerButtons;
        private Stack<Question> CSStack;
        private Topics data;
        private Stack<Question> geogStack;
        private Stack<Question> mathsStack;
        private int questionAmount;
        private int totalquestions;

        public Quiz(int id, Stats stats, Topic topic)
        {
            this.id = id;
            this.stats = stats;
            this.topic = topic;
            stats.level++;

            InitializeComponent();
            Shown += RunQuiz;
        }

        public void AskQuestion(Topic topic)
        {
            var currentQuestion = new Question();
            switch (topic)
            {
                case Topic.Geography:
                    currentQuestion = geogStack.Pop();
                    break;
                case Topic.Maths:
                    currentQuestion = mathsStack.Pop();
                    break;
                case Topic.CS:
                    currentQuestion = CSStack.Pop();
                    break;
            }

            answerButtons.ListShuffle();
            answerButtons[0].Text = currentQuestion.Answer;
            answerButtons[1].Text = currentQuestion.Distractor[0];
            answerButtons[2].Text = currentQuestion.Distractor[1];
            answerButtons[3].Text = currentQuestion.Distractor[2];
            lblQuestion.Text = currentQuestion.Questiontext;

            answerButtons[0].Click += OnCorrect;
            answerButtons[1].Click += OnIncorrect;
            answerButtons[2].Click += OnIncorrect;
            answerButtons[3].Click += OnIncorrect;
        }

        public void RunQuiz(object sender, EventArgs e)
        {
            switch (topic)
            {
                case Topic.Maths:
                    totalquestions = 3;
                    AskQuestion(Topic.Maths);
                    break;
                case Topic.Geography:
                    totalquestions = 5;
                    AskQuestion(Topic.Geography);
                    break;
                case Topic.CS:
                    totalquestions = 8;
                    AskQuestion(Topic.CS);
                    break;
            }
        }

        public void SetColour(bool answered)
        {
            if (answered)
            {
                answerButtons[0].BackColor = Color.Green;
                answerButtons[1].BackColor = Color.Red;
                answerButtons[2].BackColor = Color.Red;
                answerButtons[3].BackColor = Color.Red;
            }
            else
            {
                answerButtons[0].BackColor = Color.Gray;
                answerButtons[1].BackColor = Color.Gray;
                answerButtons[2].BackColor = Color.Gray;
                answerButtons[3].BackColor = Color.Gray;
            }
        }

        public void OnCorrect(object sender, EventArgs e)
        {
            SetColour(true);
            stats.score += 1;
            ResetEvents();
            MessageBox.Show("Answer Correct");
            SetColour(false);
            questionAmount++;
            if (questionAmount < totalquestions)
            {
                AskQuestion(topic);
            }
            else
            {
                if (topic == Topic.CS) FinishGame();

                Close();
            }
        }

        public void OnIncorrect(object sender, EventArgs e)
        {
            SetColour(true);
            ResetEvents();
            MessageBox.Show("Answer Incorrect");
            SetColour(false);
            questionAmount++;
            if (questionAmount < totalquestions)
            {
                AskQuestion(topic);
            }
            else
            {
                if (topic == Topic.CS) FinishGame();

                Close();
            }
        }

        public void ResetEvents()
        {
            btnAnswerBL.Click -= OnCorrect;
            btnAnswerBL.Click -= OnIncorrect;
            btnAnswerBR.Click -= OnCorrect;
            btnAnswerBR.Click -= OnIncorrect;
            btnAnswerTL.Click -= OnCorrect;
            btnAnswerTL.Click -= OnIncorrect;
            btnAnswerTR.Click -= OnCorrect;
            btnAnswerTR.Click -= OnIncorrect;
        }


        public void FinishGame()
        {
            var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
            var db = new Database(dbConnection);

            var score = new Score
            {
                userID = id,
                score = stats.score,
                date = DateTime.Today.ToString()
            };

            db.Scores.InsertOnSubmit(score);
            try
            {
                db.SubmitChanges();
            }
            catch (SQLiteException exception)
            {
                Console.WriteLine(exception.Data.ToString());
                Console.WriteLine(exception.ErrorCode);
                Console.WriteLine(exception.HelpLink);
                Console.WriteLine(exception.InnerException);
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.StackTrace);
                Console.WriteLine(exception.TargetSite);
                Console.WriteLine(exception.ToString());

                db.SubmitChanges();
            }
        }

        private void Quiz_Load(object sender, EventArgs e)
        {
            using (var sr = new StreamReader(@"questions.xml"))
            {
                data = (Topics) ser.Deserialize(sr);
            }

            answerButtons = new List<Button>();
            answerButtons.Add(btnAnswerBL);
            answerButtons.Add(btnAnswerBR);
            answerButtons.Add(btnAnswerTL);
            answerButtons.Add(btnAnswerTR);

            data.Geography.Question.ListShuffle();
            data.Maths.Question.ListShuffle();
            data.Cs.Question.ListShuffle();

            geogStack = new Stack<Question>(data.Geography.Question);
            mathsStack = new Stack<Question>(data.Maths.Question);
            CSStack = new Stack<Question>(data.Cs.Question);
            SetColour(false);
        }
    }
}