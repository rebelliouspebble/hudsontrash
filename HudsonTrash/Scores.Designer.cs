﻿namespace HudsonTrash
{
    partial class Scores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScoreGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize) (this.ScoreGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ScoreGrid
            // 
            this.ScoreGrid.AllowUserToAddRows = false;
            this.ScoreGrid.AllowUserToDeleteRows = false;
            this.ScoreGrid.ColumnHeadersHeightSizeMode =
                System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ScoreGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScoreGrid.Location = new System.Drawing.Point(0, 0);
            this.ScoreGrid.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ScoreGrid.Name = "ScoreGrid";
            this.ScoreGrid.ReadOnly = true;
            this.ScoreGrid.Size = new System.Drawing.Size(268, 411);
            this.ScoreGrid.TabIndex = 0;
            // 
            // Scores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 411);
            this.Controls.Add(this.ScoreGrid);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Scores";
            this.Text = "Scores";
            this.Load += new System.EventHandler(this.Scores_Load);
            ((System.ComponentModel.ISupportInitialize) (this.ScoreGrid)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.DataGridView ScoreGrid;
    }
}