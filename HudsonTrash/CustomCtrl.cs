﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace HudsonTrash
{
    public enum Direction
    {
        Right = 1,
        Left = 2,
        Up = 3,
        Down = 4,
        UpRight = 5,
        UpLeft = 6,
        DownRight = 7,
        DownLeft = 8
    }

    public class Truck : PictureBox
    {
        private Point track;

        private Point track1L;
        private Point track1R;
        private Point track2L;
        private Point track2R;
        public int Speed { get; set; }

        public Direction CurrentDirection { get; set; }

        public int OffsetX { get; set; }

        public int Track
        {
            get
            {
                if (track == track1L) return 1;
                if (track == track1R) return 2;
                if (track == track2L) return 3;
                if (track == track2R) return 4;

                return 0;
            }
            set
            {
                if (value == 1)
                    track = track1L;
                else if (value == 2)
                    track = track1R;
                else if (value == 3)
                    track = track2L;
                else if (value == 4) track = track2R;
            }
        }

        public void SetTrackLocation(Point trk1L, Point trk1R, Point trk2L, Point trk2R)
        {
            track1L = trk1L;
            track1R = trk1R;
            track2L = trk2L;
            track2R = trk2R;
        }

        public void ManageCollision(PictureBox trk1L, PictureBox trk1R, PictureBox trk2L, PictureBox trk2R)
        {
            switch (CurrentDirection)
            {
                case Direction.Right:
                    if (Bounds.IntersectsWith(trk1R.Bounds) || Bounds.IntersectsWith(trk2R.Bounds))
                    {
                        OffsetX = -100;
                        Reset();
                    }

                    break;
                case Direction.Left:
                    if (Bounds.IntersectsWith(trk1L.Bounds) || Bounds.IntersectsWith(trk2L.Bounds))
                    {
                        OffsetX = 100;
                        Reset();
                    }

                    break;
            }
        }


        public void Drive()
        {
            switch (CurrentDirection)
            {
                case Direction.Left:
                    Location = new Point(Location.X - Speed, Location.Y);
                    break;
                case Direction.Right:
                    Location = new Point(Location.X + Speed, Location.Y);
                    break;
            }
        }

        public void Reset()
        {
            Location = new Point(track.X + OffsetX, track.Y);
        }
    }

    public class TrashCan : PictureBox
    {
        public int Inventory { get; set; }
    }

    public class Trash : PictureBox
    {
        private readonly RNGCryptoServiceProvider Rand =
            new RNGCryptoServiceProvider();

        private Random random = new Random();
        public int XLower { get; set; }
        public int XHigher { get; set; }
        public int YLower { get; set; }
        public int YHigher { get; set; }

        // Return a random integer between a min and max value.
        private int RandomInteger(int min, int max)
        {
            var scale = uint.MaxValue;
            while (scale == uint.MaxValue)
            {
                // Get four random bytes.
                var four_bytes = new byte[4];
                Rand.GetBytes(four_bytes);

                // Convert that into an uint.
                scale = BitConverter.ToUInt32(four_bytes, 0);
            }

            // Add min to the scaled difference between max and min.
            return (int) (min + (max - min) *
                          (scale / (double) uint.MaxValue));
        }


        public void Reset()
        {
            BackColor = Color.Crimson;
            var newx = 0;
            var newy = 0;
            do
            {
                newx = RandomInteger(XLower, XHigher);
                newy = RandomInteger(YLower, YHigher);

                Debug.Print("Generated X = " + newx + ", Generated Y = " + newy);
            } while (XLower > newx && newx > XHigher && YLower > newy && newy > YHigher);

            Location = new Point(newx, newy);
            Debug.Print("Trash Initialised");
        }
    }

    public class Hudson : PictureBox
    {
        private Point initialPoint;

        public int Life { get; set; }

        public int Inventory { get; set; }

        public int FormWidth { get; set; }

        public int FormHeight { get; set; }

        public int Speed { get; set; }

        public void setInitialPoint()
        {
            initialPoint = Location;
        }

        public void Reset()
        {
            Life -= 1;
            Location = initialPoint;
            Inventory = 0;
        }

        public void Move(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    Top -= Speed;
                    break;
                case Direction.Down:
                    Top += Speed;
                    break;
                case Direction.Left:
                    Left -= Speed;
                    break;
                case Direction.Right:
                    Left += Speed;
                    break;
                case Direction.UpLeft:
                    Top -= Speed / 2;
                    Left -= Speed / 2;
                    break;
                case Direction.UpRight:
                    Top -= Speed / 2;
                    Left += Speed / 2;
                    break;
                case Direction.DownLeft:
                    Top += Speed / 2;
                    Left -= Speed / 2;
                    break;
                case Direction.DownRight:
                    Top += Speed / 2;
                    Left += Speed / 2;
                    break;
            }

            Top = Math.Min(Math.Max(Top, Height / 5), FormHeight - Height * 2);
            Left = Math.Min(Math.Max(Left, Width / 5), FormWidth - Width * 2);
        }
    }
}