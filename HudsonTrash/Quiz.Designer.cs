﻿namespace HudsonTrash
{
    partial class Quiz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnswerTL = new System.Windows.Forms.Button();
            this.btnAnswerBL = new System.Windows.Forms.Button();
            this.btnAnswerTR = new System.Windows.Forms.Button();
            this.btnAnswerBR = new System.Windows.Forms.Button();
            this.lblQuestion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAnswerTL
            // 
            this.btnAnswerTL.Location = new System.Drawing.Point(57, 215);
            this.btnAnswerTL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnswerTL.Name = "btnAnswerTL";
            this.btnAnswerTL.Size = new System.Drawing.Size(376, 143);
            this.btnAnswerTL.TabIndex = 0;
            this.btnAnswerTL.Text = "button1";
            this.btnAnswerTL.UseVisualStyleBackColor = true;
            // 
            // btnAnswerBL
            // 
            this.btnAnswerBL.Location = new System.Drawing.Point(57, 363);
            this.btnAnswerBL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnswerBL.Name = "btnAnswerBL";
            this.btnAnswerBL.Size = new System.Drawing.Size(376, 143);
            this.btnAnswerBL.TabIndex = 1;
            this.btnAnswerBL.Text = "button2";
            this.btnAnswerBL.UseVisualStyleBackColor = true;
            // 
            // btnAnswerTR
            // 
            this.btnAnswerTR.Location = new System.Drawing.Point(493, 215);
            this.btnAnswerTR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnswerTR.Name = "btnAnswerTR";
            this.btnAnswerTR.Size = new System.Drawing.Size(376, 143);
            this.btnAnswerTR.TabIndex = 2;
            this.btnAnswerTR.Text = "button3";
            this.btnAnswerTR.UseVisualStyleBackColor = true;
            // 
            // btnAnswerBR
            // 
            this.btnAnswerBR.Location = new System.Drawing.Point(493, 363);
            this.btnAnswerBR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnswerBR.Name = "btnAnswerBR";
            this.btnAnswerBR.Size = new System.Drawing.Size(376, 143);
            this.btnAnswerBR.TabIndex = 3;
            this.btnAnswerBR.Text = "button4";
            this.btnAnswerBR.UseVisualStyleBackColor = true;
            // 
            // lblQuestion
            // 
            this.lblQuestion.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblQuestion.Location = new System.Drawing.Point(144, 53);
            this.lblQuestion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuestion.Name = "lblQuestion";
            this.lblQuestion.Size = new System.Drawing.Size(673, 119);
            this.lblQuestion.TabIndex = 4;
            this.lblQuestion.Text = "Question";
            this.lblQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Quiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 519);
            this.Controls.Add(this.lblQuestion);
            this.Controls.Add(this.btnAnswerBR);
            this.Controls.Add(this.btnAnswerTR);
            this.Controls.Add(this.btnAnswerBL);
            this.Controls.Add(this.btnAnswerTL);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Quiz";
            this.Text = "Quiz";
            this.Load += new System.EventHandler(this.Quiz_Load);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btnAnswerTL;
        private System.Windows.Forms.Button btnAnswerBL;
        private System.Windows.Forms.Button btnAnswerTR;
        private System.Windows.Forms.Button btnAnswerBR;
        private System.Windows.Forms.Label lblQuestion;
    }
}