﻿namespace HudsonTrash
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.pbRoad1 = new System.Windows.Forms.PictureBox();
            this.pbRoad2 = new System.Windows.Forms.PictureBox();
            this.Hudson = new HudsonTrash.Hudson();
            this.TrashCan = new HudsonTrash.TrashCan();
            this.truck1 = new HudsonTrash.Truck();
            this.truck2 = new HudsonTrash.Truck();
            this.truck3 = new HudsonTrash.Truck();
            this.track1L = new System.Windows.Forms.PictureBox();
            this.track2L = new System.Windows.Forms.PictureBox();
            this.track2R = new System.Windows.Forms.PictureBox();
            this.track1R = new System.Windows.Forms.PictureBox();
            this.DurationTimer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLives = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hudson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrashCan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1R)).BeginInit();
            this.SuspendLayout();
            // 
            // pbRoad1
            // 
            this.pbRoad1.BackColor = System.Drawing.Color.DimGray;
            this.pbRoad1.Location = new System.Drawing.Point(1, 98);
            this.pbRoad1.Name = "pbRoad1";
            this.pbRoad1.Size = new System.Drawing.Size(799, 87);
            this.pbRoad1.TabIndex = 0;
            this.pbRoad1.TabStop = false;
            this.pbRoad1.Click += new System.EventHandler(this.pbRoad1_Click);
            // 
            // pbRoad2
            // 
            this.pbRoad2.BackColor = System.Drawing.Color.DimGray;
            this.pbRoad2.Location = new System.Drawing.Point(1, 243);
            this.pbRoad2.Name = "pbRoad2";
            this.pbRoad2.Size = new System.Drawing.Size(799, 87);
            this.pbRoad2.TabIndex = 1;
            this.pbRoad2.TabStop = false;
            this.pbRoad2.Click += new System.EventHandler(this.pbRoad2_Click);
            // 
            // Hudson
            // 
            this.Hudson.BackColor = System.Drawing.Color.DarkRed;
            this.Hudson.FormHeight = 0;
            this.Hudson.FormWidth = 0;
            this.Hudson.Inventory = 0;
            this.Hudson.Life = 0;
            this.Hudson.Location = new System.Drawing.Point(364, 378);
            this.Hudson.Name = "Hudson";
            this.Hudson.Size = new System.Drawing.Size(76, 45);
            this.Hudson.Speed = 0;
            this.Hudson.TabIndex = 2;
            this.Hudson.TabStop = false;
            // 
            // TrashCan
            // 
            this.TrashCan.BackColor = System.Drawing.Color.Cyan;
            this.TrashCan.Inventory = 0;
            this.TrashCan.Location = new System.Drawing.Point(351, 21);
            this.TrashCan.Name = "TrashCan";
            this.TrashCan.Size = new System.Drawing.Size(100, 50);
            this.TrashCan.TabIndex = 3;
            this.TrashCan.TabStop = false;
            // 
            // truck1
            // 
            this.truck1.BackColor = System.Drawing.Color.Black;
            this.truck1.Location = new System.Drawing.Point(351, 268);
            this.truck1.Name = "truck1";
            this.truck1.OffsetX = 0;
            this.truck1.Size = new System.Drawing.Size(100, 50);
            this.truck1.Speed = 0;
            this.truck1.TabIndex = 4;
            this.truck1.TabStop = false;
            this.truck1.Track = 1;
            // 
            // truck2
            // 
            this.truck2.Location = new System.Drawing.Point(43, 114);
            this.truck2.Name = "truck2";
            this.truck2.OffsetX = 0;
            this.truck2.Size = new System.Drawing.Size(100, 50);
            this.truck2.Speed = 0;
            this.truck2.TabIndex = 5;
            this.truck2.TabStop = false;
            this.truck2.Track = 1;
            // 
            // truck3
            // 
            this.truck3.Location = new System.Drawing.Point(650, 114);
            this.truck3.Name = "truck3";
            this.truck3.OffsetX = 0;
            this.truck3.Size = new System.Drawing.Size(100, 50);
            this.truck3.Speed = 0;
            this.truck3.TabIndex = 6;
            this.truck3.TabStop = false;
            this.truck3.Track = 1;
            // 
            // track1L
            // 
            this.track1L.BackColor = System.Drawing.Color.DimGray;
            this.track1L.Location = new System.Drawing.Point(1, 114);
            this.track1L.Name = "track1L";
            this.track1L.Size = new System.Drawing.Size(7, 7);
            this.track1L.TabIndex = 7;
            this.track1L.TabStop = false;
            // 
            // track2L
            // 
            this.track2L.BackColor = System.Drawing.Color.DimGray;
            this.track2L.Location = new System.Drawing.Point(1, 268);
            this.track2L.Name = "track2L";
            this.track2L.Size = new System.Drawing.Size(7, 7);
            this.track2L.TabIndex = 8;
            this.track2L.TabStop = false;
            // 
            // track2R
            // 
            this.track2R.BackColor = System.Drawing.Color.DimGray;
            this.track2R.Location = new System.Drawing.Point(793, 268);
            this.track2R.Name = "track2R";
            this.track2R.Size = new System.Drawing.Size(7, 7);
            this.track2R.TabIndex = 9;
            this.track2R.TabStop = false;
            // 
            // track1R
            // 
            this.track1R.BackColor = System.Drawing.Color.DimGray;
            this.track1R.Location = new System.Drawing.Point(793, 114);
            this.track1R.Name = "track1R";
            this.track1R.Size = new System.Drawing.Size(7, 7);
            this.track1R.TabIndex = 10;
            this.track1R.TabStop = false;
            // 
            // DurationTimer
            // 
            this.DurationTimer.Interval = 1000;
            this.DurationTimer.Tick += new System.EventHandler(this.DurationTimer_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 410);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Time Remaining:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 428);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Trash Collected:";
            // 
            // lblLives
            // 
            this.lblLives.AutoSize = true;
            this.lblLives.Location = new System.Drawing.Point(13, 394);
            this.lblLives.Name = "lblLives";
            this.lblLives.Size = new System.Drawing.Size(88, 13);
            this.lblLives.TabIndex = 13;
            this.lblLives.Text = "Lives Remaining:";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblLives);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.track1R);
            this.Controls.Add(this.track2R);
            this.Controls.Add(this.track2L);
            this.Controls.Add(this.track1L);
            this.Controls.Add(this.truck3);
            this.Controls.Add(this.truck2);
            this.Controls.Add(this.truck1);
            this.Controls.Add(this.TrashCan);
            this.Controls.Add(this.Hudson);
            this.Controls.Add(this.pbRoad2);
            this.Controls.Add(this.pbRoad1);
            this.Name = "Game";
            this.Text = "Game";
            this.Load += new System.EventHandler(this.Game_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Game_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRoad2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hudson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrashCan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.truck3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track2R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track1R)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.PictureBox pbRoad2;
        private System.Windows.Forms.PictureBox pbRoad1;
        private HudsonTrash.Hudson Hudson;
        private HudsonTrash.TrashCan TrashCan;
        private HudsonTrash.Truck truck2;
        private HudsonTrash.Truck truck3;
        private System.Windows.Forms.PictureBox track1L;
        private System.Windows.Forms.PictureBox track2L;
        private System.Windows.Forms.PictureBox track2R;
        private System.Windows.Forms.PictureBox track1R;
        private HudsonTrash.Truck truck1;
        private System.Windows.Forms.Timer DurationTimer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLives;
    }
}