using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SQLite;

namespace HudsonTrash
{
    [Table(Name = "Users")]
    public class User
    {
        [Column(IsPrimaryKey = true)] public int? id { get; set; }

        [Column] public string username { get; set; }

        [Column] public string password { get; set; }
    }

    [Table(Name = "Scores")]
    public class Score
    {
        [Column(IsPrimaryKey = true)] public int? id { get; set; }

        [Column] public int userID { get; set; }

        [Column] public string date { get; set; }

        [Column] public int score { get; set; }
    }

    [Database]
    public class Database : DataContext
    {
        public Table<Score> Scores;
        public Table<User> Users;

        public Database(SQLiteConnection connection) : base(connection)
        {
        }
    }
}