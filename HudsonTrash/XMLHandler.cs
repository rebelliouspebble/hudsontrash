using System.Collections.Generic;
using System.Xml.Serialization;

namespace HudsonTrash
{
    [XmlRoot(ElementName = "question")]
    public class Question
    {
        [XmlElement(ElementName = "questiontext")]
        public string Questiontext { get; set; }

        [XmlElement(ElementName = "answer")] public string Answer { get; set; }

        [XmlElement(ElementName = "distractor")]
        public List<string> Distractor { get; set; }
    }

    [XmlRoot(ElementName = "geography")]
    public class Geography
    {
        [XmlElement(ElementName = "question")] public List<Question> Question { get; set; }
    }

    [XmlRoot(ElementName = "maths")]
    public class Maths
    {
        [XmlElement(ElementName = "question")] public List<Question> Question { get; set; }
    }

    [XmlRoot(ElementName = "cs")]
    public class Cs
    {
        [XmlElement(ElementName = "question")] public List<Question> Question { get; set; }
    }

    [XmlRoot(ElementName = "topics")]
    public class Topics
    {
        [XmlElement(ElementName = "geography")]
        public Geography Geography { get; set; }

        [XmlElement(ElementName = "maths")] public Maths Maths { get; set; }

        [XmlElement(ElementName = "cs")] public Cs Cs { get; set; }
    }
}