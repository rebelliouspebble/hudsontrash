﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace HudsonTrash
{
    public partial class Game : Form
    {
        private readonly int id;
        private readonly Stats stats = new Stats();
        private readonly List<Trash> trash = new List<Trash>();
        private int timeElapsed;
        private double totalTime;

        private int trashNeeded;

        public Game(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void ResetGame(object sender, EventArgs e)
        {
            TrashCan.Inventory = 0;
            if (stats.level == 2)
            {
                initTrucks(10);
                initHudson();
                Hudson.Life = 3;
                Hudson.Reset();
                initTrash(3);
                trashNeeded = 8;
                totalTime = 90 + stats.score * 10;
                timeElapsed = 0;
                DurationTimer.Start();
                GameTimer.Start();
            }
            else
            {
                initTrucks(15);
                initHudson();
                Hudson.Life = 3;
                Hudson.Reset();
                initTrash(2);
                trashNeeded = 10;
                totalTime = 90 + stats.score * 10;
                timeElapsed = 0;
                DurationTimer.Start();
                GameTimer.Start();
            }
        }

        private void TimerTick(object sender, EventArgs e)
        {
            truck1.Drive();
            truck2.Drive();
            truck3.Drive();
            truck1.ManageCollision(track1L, track1R, track2L, track2R);
            truck2.ManageCollision(track1L, track1R, track2L, track2R);
            truck3.ManageCollision(track1L, track1R, track2L, track2R);


            if (Hudson.Bounds.IntersectsWith(truck1.Bounds) || Hudson.Bounds.IntersectsWith(truck2.Bounds) ||
                Hudson.Bounds.IntersectsWith(truck3.Bounds))
            {
                Hudson.Reset();
                lblLives.Text = "Lives Remaining: " + Hudson.Life;
                if (Hudson.Life == 0)
                {
                    MessageBox.Show("You Lost!");
                    Close();
                }
            }

            if (Hudson.Bounds.IntersectsWith(TrashCan.Bounds))
            {
                TrashCan.Inventory += Hudson.Inventory;
                Hudson.Inventory = 0;
                label2.Text = "Trash Collected: " + Hudson.Inventory;


                if (TrashCan.Inventory >= trashNeeded)
                {
                    Debug.Print("Trash Full");
                    GameTimer.Stop();
                    DurationTimer.Stop();
                    Hide();
                    Quiz Quiz;
                    switch (stats.level)
                    {
                        case 1:
                            Quiz = new Quiz(id, stats, Quiz.Topic.Maths);
                            break;
                        case 2:
                            Quiz = new Quiz(id, stats, Quiz.Topic.Geography);
                            break;
                        case 3:
                            Quiz = new Quiz(id, stats, Quiz.Topic.CS);
                            break;
                        default:
                            Quiz = new Quiz(id, stats, Quiz.Topic.Maths);
                            break;
                    }

                    Quiz.Closed += (s, args) => Show();
                    Quiz.Closed += ResetGame;

                    if (stats.level == 4) Quiz.Closed += (s, args) => Close();

                    Quiz.Show();
                }
            }

            foreach (var i in trash)
                if (Hudson.Bounds.IntersectsWith(i.Bounds))
                    if (Hudson.Inventory < 3)
                    {
                        Hudson.Inventory += 1;
                        label2.Text = "Trash Collected: " + Hudson.Inventory;
                        i.Reset();
                    }
        }

        private void pbRoad1_Click(object sender, EventArgs e)
        {
            Debug.Print(MousePosition.ToString());
        }

        private void pbRoad2_Click(object sender, EventArgs e)
        {
            Debug.Print(MousePosition.ToString());
        }

        private void initHudson()
        {
            Hudson.Speed = 4;
            Hudson.FormHeight = Height;
            Hudson.FormWidth = Width;
            Hudson.Life = 3;
            Hudson.Inventory = 0;
        }

        private void initTrucks(int speed)
        {
            truck1.SetTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
            truck2.SetTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
            truck3.SetTrackLocation(track1L.Location, track1R.Location, track2L.Location, track2R.Location);
            truck1.Track = 1;
            truck2.Track = 1;
            truck3.Track = 4;
            truck1.CurrentDirection = Direction.Right;
            truck2.CurrentDirection = Direction.Right;
            truck3.CurrentDirection = Direction.Left;
            truck1.Speed = speed;
            truck2.Speed = speed;
            truck3.Speed = speed;
            truck2.OffsetX = 200;
            truck1.Reset();
            truck2.Reset();
            truck3.Reset();
        }


        private void initTrash(int amount)
        {
            if (amount <= 0 || amount > 5) throw new ArgumentOutOfRangeException(nameof(amount));
            var xlower = Bounds.Location.X + 100;
            Debug.Print("X-Lower: " + xlower);
            var xhigher = Bounds.Size.Width + Bounds.Location.X - 200;
            Debug.Print("X-Higher: " + xhigher);

            var ylower = Bounds.Location.Y + 100;
            Debug.Print("Y-Lower: " + ylower);
            var yhigher = Bounds.Size.Height + Bounds.Location.Y - 200;
            Debug.Print("Y-Higher: " + yhigher);

            for (var i = 0; i < amount; i++) trash.Add(new Trash());

            foreach (var i in trash)
            {
                i.XLower = xlower;
                i.XHigher = xhigher;
                i.YLower = ylower;
                i.YHigher = yhigher;
                i.Reset();
                i.Size = new Size(64, 64);
                Controls.Add(i);
                i.BringToFront();
            }
        }

        private void Game_Load(object sender, EventArgs e)
        {
            initHudson();
            initTrucks(5);
            initTrash(5);
            truck1.BringToFront();
            truck2.BringToFront();
            truck3.BringToFront();
            Hudson.setInitialPoint();
            timeElapsed = 0;
            totalTime = 90;
            TrashCan.Inventory = 0;
            lblLives.Text = "Lives Remaining: 3";
            pbRoad1.SendToBack();
            pbRoad2.SendToBack();
            GameTimer.Interval = 50;
            GameTimer.Tick += TimerTick;
            GameTimer.Start();
            DurationTimer.Start();
        }

        private void Game_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                    Hudson.Move(Direction.Up);
                    break;
                case Keys.A:
                    Hudson.Move(Direction.Left);
                    break;
                case Keys.S:
                    Hudson.Move(Direction.Down);
                    break;
                case Keys.D:
                    Hudson.Move(Direction.Right);
                    break;
            }
        }

        private void DurationTimer_Tick(object sender, EventArgs e)
        {
            timeElapsed += 1;
            if (timeElapsed == totalTime)
            {
                MessageBox.Show("You Lost!");
                Close();
            }

            label1.Text = "Time Remaining: " + (totalTime - timeElapsed);
        }
    }
}