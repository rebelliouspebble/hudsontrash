﻿using System;
using System.Windows.Forms;

namespace HudsonTrash
{
    public partial class Landing : Form
    {
        private readonly int id;

        public Landing(int id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void btnScores_Click(object sender, EventArgs e)
        {
            Hide();
            var Scores = new Scores(id);
            Scores.Closed += (s, args) => Show();
            Scores.Show();
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnPlay_Click(object sender, EventArgs e)
        {
            Hide();
            var Game = new Game(id);
            Game.Closed += (s, args) => Show();
            Game.Show();
        }
    }
}